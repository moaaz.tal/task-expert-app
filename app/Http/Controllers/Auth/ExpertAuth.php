<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginExpertRequest;
use App\Http\Requests\RegisterExpertRequest;
use App\Models\ApprovalStatus;
use App\Models\CustomSecvices;
use App\Models\Expert;
use App\Models\ExpertSevice;
use App\Models\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Validator;

class ExpertAuth extends Controller
{
    public function expertRegister(Request $request)
    {
        DB::beginTransaction();
        try {
            $registerRequest = new RegisterExpertRequest;
            $validate = Validator::make($request->all(), $registerRequest->rules());
            if ($validate->fails()) {
                return $this->sendError("Vlidation Errors", $validate->errors()->toArray(), 422);
            }

            $input = $request->all();

            $custom_services = json_decode($input['custom_services']);
            foreach ($custom_services as $custom_service) {
                if (!CustomSecvices::where('custom_secvice_id', $custom_service)->exists()) {
                    return $this->sendError("Vlidation Errors", ['CustomSecvices' => ["the $custom_service is not found"]], 422);
                }
            }

            $input['password'] = bcrypt($request->password);

            $profile_photo = $request->profile_photo;
            $new_profile_photo = time() . rand(1, 100000) . '.' . $profile_photo->getClientOriginalExtension();
            $profile_photo->move('uploads/profile_photo', $new_profile_photo);
            $input['profile_photo'] = "/uploads/profile_photo/$new_profile_photo";

            $id_photo = $request->id_photo;
            $new_id_photo = time() . rand(1, 100000) . '.' . $id_photo->getClientOriginalExtension();
            $id_photo->move('uploads/id_photo', $new_id_photo);
            $input['id_photo'] = "/uploads/id_photo/$new_id_photo";


            $role_user = Role::findByName('user');
            $user = User::create($input);
            $user->assignRole($role_user);


            $input['user_id'] = $user->user_id;
            $approval_status = ApprovalStatus::where('name', 'pending')->first();
            $input['approval_status_id'] = $approval_status->approval_status_id;
            $expert = Expert::create($input);
            $expert->user;
            foreach ($custom_services as $custom_service) {
                ExpertSevice::create([
                    "custom_secvice_id" => $custom_service,
                    "expert_id" => $expert->expert_id
                ]);
            }

            DB::commit();

            return $this->sendSuccess("registered successfully, please wait to accept", $expert, 201);

        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->sendError('something went wrong on our end', ['error' => [$ex->getMessage()]], 500);
        }
    }

    public function loginExpert(Request $request)
    {
        $loginRequest = new LoginExpertRequest;

        $validate = Validator::make($request->all(), $loginRequest->rules());
        if ($validate->fails()) {
            return $this->sendError("Vlidation Errors", $validate->errors()->toArray(), 422);
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            if (!$loginRequest->authorize()) {

                return $this->sendError("unauthorized", "you arn't expert now", 401);
            }
            $user = auth()->user();
            $user->token = $user->createToken('make it scure')->accessToken;
            return $this->sendSuccess('expert Login Successfully!', $user);
        }
        return $this->sendError("Vlidation Errors", ["password" => ["incorrect password"]], 401);
    }

}