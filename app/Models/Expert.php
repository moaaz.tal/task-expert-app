<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expert extends Model
{
    use HasFactory;
    protected $primaryKey = "expert_id";

    protected $guarded = [
        'expert_id'
    ];
    public function expert_service()
    {
        return $this->hasMany(ExpertSevice::class, "expert_id", "expert_id");
    }

    public function approval_status()
    {
        return $this->belongsTo(ApprovalStatus::class, "approval_status_id", "approval_status_id");
    }

    public function user()
    {
        return $this->belongsTo(User::class, "user_id", "user_id");
    }
}
