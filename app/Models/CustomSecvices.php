<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomSecvices extends Model
{
    use HasFactory;
    protected $primaryKey = "custom_secvice_id";

    protected $guarded = [
        'custom_secvice_id'
    ];
    public function expert_service()
    {
        return $this->hasMany(ExpertSevice::class, "custom_secvice_id", "custom_secvice_id");
    }

    public function general_services()
    {
        return $this->belongsTo(GeneralServices::class, "general_service_id", "general_service_id");
    }

}

