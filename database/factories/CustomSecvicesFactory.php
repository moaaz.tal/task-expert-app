<?php

namespace Database\Factories;

use App\Models\GeneralServices;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CustomSecvices>
 */
class CustomSecvicesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'general_service_id' => function () {
                $temp = GeneralServices::all()->toArray();
                if (is_null($temp)) {
                    GeneralServices::factory()->count(1)->create();
                    return 1;
                }
                $rand_index = array_rand($temp);
                $ret = $temp[$rand_index]['general_service_id'];
                return $ret;
            },
            "name"=>fake()->text(20)
        ];
    }
}
