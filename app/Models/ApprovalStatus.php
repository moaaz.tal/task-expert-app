<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApprovalStatus extends Model
{
    use HasFactory;
    protected $primaryKey = "user_id";

    protected $guarded = [
        'user_id'
    ];

    public function expert()
    {
        return $this->hasMany(Expert::class, "approval_status_id", "approval_status_id");
    }
}
