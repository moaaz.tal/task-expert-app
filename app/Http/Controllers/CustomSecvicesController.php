<?php

namespace App\Http\Controllers;

use App\Models\CustomSecvices;
use App\Http\Requests\StoreCustomSecvicesRequest;
use App\Http\Requests\UpdateCustomSecvicesRequest;
use App\Models\GeneralServices;

class CustomSecvicesController extends Controller
{
    public function index()
    {
        $general = GeneralServices::get();
        $res=[];
        foreach($general as $item)
        {
            if(CustomSecvices::where('general_service_id',$item->general_service_id)->exists())
            {
                $res["$item->name"]=CustomSecvices::where('general_service_id',$item->general_service_id)->get();
            }
        }
        return $this->sendSuccess('all Services return Successfully!', $res);
    }

    public function index_for_specific_general($general_id)
    {
        if(!GeneralServices::where('general_service_id',$general_id)->exists())
        {
            return $this->sendError('there is not a general service has this id', null, 404);
        }
        $res = CustomSecvices::where('general_service_id',$general_id)->get();
        return $this->sendSuccess('all Custom Secvices for this general return Successfully!', $res);
    }

}
