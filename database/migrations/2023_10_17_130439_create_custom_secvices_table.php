<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('custom_secvices', function (Blueprint $table) {
            $table->id('custom_secvice_id');
            $table->foreignId("general_service_id")->constrained("general_services", "general_service_id")->onDelete("cascade");

            $table->string('name');
            $table->timestamps();
        });
        
        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_secvices');
    }
};
