<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('experts', function (Blueprint $table) {
            $table->id('expert_id');
            $table->foreignId("user_id")->unique()->constrained("users", "user_id")->onDelete("cascade");
            $table->foreignId("approval_status_id")->constrained("approval_statuses", "approval_status_id")->onDelete("cascade");

            $table->string('city');
            $table->string('bank');
            $table->integer('bank_account_id');
            $table->string('location');
            $table->string('profile_photo');
            $table->string('id_photo');
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experts');
    }
};
