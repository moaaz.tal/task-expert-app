<?php

namespace App\Http\Controllers;

use App\Models\GeneralServices;
use App\Http\Requests\StoreGeneralServicesRequest;
use App\Http\Requests\UpdateGeneralServicesRequest;

class GeneralServicesController extends Controller
{
    public function index()
    {
        $items = GeneralServices::all();
        return $this->sendSuccess('General Services return Successfully!', $items);
    }
}
