<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('expert_sevices', function (Blueprint $table) {
            $table->id('expert_sevice_id');
            $table->foreignId("expert_id")->constrained("experts", "expert_id")->onDelete("cascade");
            $table->foreignId("custom_secvice_id")->constrained("custom_secvices", "custom_secvice_id")->onDelete("cascade");

            $table->timestamps();
        });

        Schema::disableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expert_sevices');
    }
};
