<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterExpertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => "required",
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|max:20',
            'c_password' => 'required|same:password',
            'phone' => 'required|numeric',
            'city' => 'required',
            'location' => 'required',
            'bank' => 'required',
            'bank_account_id' => 'required|numeric',
            'profile_photo' => 'required|image',
            'id_photo' => 'required|image',
            'custom_services' => 'required|json',
        ];
    }
}
