<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GeneralServices extends Model
{
    use HasFactory;

    protected $primaryKey = "general_service_id";

    protected $guarded = [
        'general_service_id'
    ];
    public function custom_services()
    {
        return $this->hasMany(CustomSecvices::class, "general_service_id", "general_service_id");
    }

}
