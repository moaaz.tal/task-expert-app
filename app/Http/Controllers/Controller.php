<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function sendSuccess($message, $data, $status_code = 200)
    {
        return response()->json([
            "success" => True,
            "message" => $message,
            "data" => $data,
        ], $status_code);
    }

    public function sendError($message, $erros, $status_code = 400)
    {
        return response()->json([
            "success" => false,
            "message" => $message,
            "errors" => $erros,
        ], $status_code);
    }

}
