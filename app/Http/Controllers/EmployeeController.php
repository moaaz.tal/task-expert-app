<?php

namespace App\Http\Controllers;

use App\Models\ApprovalStatus;
use App\Models\Employee;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use App\Models\Expert;

class EmployeeController extends Controller
{

    public function accept_expert($expert_id)
    {
        if (!auth()->user()->hasRole('employee')) {
            return $this->sendError("unauthorized", ["error" => "you can't do that"], 401);
        }
        if (!Expert::where('expert_id', $expert_id)->exists()) {
            return $this->sendError("not found", ["expert_id" => "there isn't expert with this id"], 404);
        }

        $expert = Expert::find($expert_id);
        if ($expert->approval_status->name != 'pending') {
            return $this->sendError("validation error", ["expert_id" => "this expert is accepted or rejected already"], 400);
        }

        $expert->approval_status_id = ApprovalStatus::where('name', "accepted")->first()->approval_status_id;

        $expert->save();

        return $this->sendSuccess("expert accept successfully", $expert);
    }
    public function reject_expert($expert_id)
    {
        if (!auth()->user()->hasRole('employee')) {
            return $this->sendError("unauthorized", ["error" => "you can't do that"], 401);
        }
        if (!Expert::where('expert_id', $expert_id)->exists()) {
            return $this->sendError("not found", ["expert_id" => "there isn't expert with this id"], 404);
        }

        $expert = Expert::find($expert_id);
        if ($expert->approval_status->name != 'pending') {
            return $this->sendError("validation error", ["expert_id" => "this expert is accepted or rejected already"], 400);
        }

        $expert->approval_status_id = ApprovalStatus::where('name', "rejected")->first()->approval_status_id;

        $expert->save();

        return $this->sendSuccess("expert reject successfully", $expert);
    }

    public function pending_expert()
    {
        if (!auth()->user()->hasRole('employee')) {
            return $this->sendError("unauthorized", ["error" => "you can't do that"], 401);
        }
        $pending_id = ApprovalStatus::where("name", "pending")->first()->approval_status_id;
        $res = Expert::where('approval_status_id', $pending_id)->get();
        return $this->sendSuccess("all pending experts return successfully", $res);
    }
}