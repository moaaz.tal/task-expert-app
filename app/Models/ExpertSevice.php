<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExpertSevice extends Model
{
    use HasFactory;
    protected $primaryKey = "expert_sevice_id";

    protected $guarded = [
        'expert_sevice_id'
    ];
    public function expert()
    {
        return $this->belongsTo(Expert::class, "expert_id", "expert_id");
    }

    public function CustomSecvices()
    {
        return $this->belongsTo(CustomSecvices::class, "custom_secvice_id", "custom_secvice_id");
    }
}
