<?php

use App\Http\Controllers\Auth\ExpertAuth;
use App\Http\Controllers\CustomSecvicesController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\GeneralServicesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/expertRegister', [ExpertAuth::class, 'expertRegister']);
Route::post('/expertLogin', [ExpertAuth::class, 'loginExpert']);
Route::get('/CustomSecvices/index', [CustomSecvicesController::class, 'index']);
Route::get('/CustomSecvices/{general_id}', [CustomSecvicesController::class, 'index_for_specific_general']);
Route::get('/GeneralServices/index', [GeneralServicesController::class, 'index']);

Route::middleware(['auth:api'])->group(function () {

Route::get('/accept_expert/{expert_id}', [EmployeeController::class, 'accept_expert']);
Route::get('/reject_expert/{expert_id}', [EmployeeController::class, 'reject_expert']);
Route::get('/pending_expert', [EmployeeController::class, 'pending_expert']);
});