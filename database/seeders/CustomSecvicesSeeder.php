<?php

namespace Database\Seeders;

use App\Models\CustomSecvices;
use App\Models\GeneralServices;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CustomSecvicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $temp = GeneralServices::all();
        foreach($temp as $item)
        {
            CustomSecvices::factory()->count(6)->create(['general_service_id' => $item->general_service_id]);
        }
    }
}
