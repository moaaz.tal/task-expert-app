<?php

namespace Database\Seeders;

use App\Models\ApprovalStatus;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ApprovalStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         ApprovalStatus::create(['name' => 'pending']);
         ApprovalStatus::create(['name' => 'accepted']);
         ApprovalStatus::create(['name' => 'rejected']);
    }
}
